Pod::Spec.new do |s|
  s.name         = "IISUIBasicsKit"
  s.version      = "0.0.1"
  s.summary      = "IISUIBasicsKit is a collection of basic UI stuff..."
  s.description  = "Provider longer and better description for IISUIBasicsKit (tbd)"
  s.homepage     = "http://www.iis-labs.com"
  s.license      = "MIT (example)"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "IIS New Media Labs e.U." => "contact@iis-labs.com" }
  s.social_media_url   = "http://twitter.com/iislabs"
  s.platform     = :ios, "8.0"
  s.source       = { :git => "http://www.bitbucket.com/hschmied/iisuibasicskit.git", :tag => s.version }
  s.source_files  = "IISUIBasicsKit/**/*.swift"
#s.exclude_files = "Classes/Exclude"
  s.framework  = "UIKit"
  s.requires_arc = true
  s.dependency "BetterBaseClasses", "~> 1.6.2"

end
