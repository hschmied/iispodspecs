#
#  Be sure to run `pod spec lint IISTableUIKit.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name         = "IISTableUIKit"
  s.version      = "0.1.0"
  s.summary      = "IISTableUIKit is a UIKit extension for everything tableview related -- tableview, cells,..."
  s.description  = "IISTableUIKit is a UIKit extension for everything tableview related -- tableview, cells,... (tbd)"
  s.homepage     = "http://www.iis-labs.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "IIS Labs e.U." => "contact@iis-labs.com" }
  s.social_media_url   = "http://twitter.com/iislabs"
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://bitbucket.org/hschmied/iistableuikit.git", :tag => "0.1.0" }
  s.source_files = "IISTableUIKit/**/*.swift"
  s.source_files = "IISTableUIKit/**/*.{h,m}"

  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  s.framework  = "UIKit"
  # s.frameworks = "SomeFramework", "AnotherFramework"

  # s.library   = "iconv"
  # s.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  # s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
